stdin = ["rouge", "rouge", "vert", "jaune", "vert", "rouge"] * 10

lines = []
for line in stdin:
    lines.append(line.strip('\n'))

print(lines)

lines = [line.strip() for line in stdin]

print(lines)

lines = [line for line in stdin if line not in ["rouge"]]

print(lines)

lines = [line1 + line2 for line1 in stdin for line2 in stdin]

print(lines)

lines = [line1 + line2 for line1, line2 in zip(stdin, stdin) if line1 not in ["jaune", "rouge"]]

print(lines)
