def fct(param1, param2, param3, **kwargs):
    print(param1, param2, param3, kwargs)


array = list(range(10))
date = '23 november 2023'

day, month, year = date.split()

a, *b = array
c, *d, e = array

parameters = {
    'param3': 'value_3',
    'param1': 1,
    'param2': False,
    'param5': 'value 5',
    'param4': (0, 1, 2)
}

fct(**parameters)

