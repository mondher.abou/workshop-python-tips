from sys import stderr

values = [str(i) for i in range(10)]

print("Hello world")

print(values)

print(*values)

print(*values, sep=' & ')

print(*values, sep=' & ', end='')
print('2nd print')

print(*values, file=stderr)

with open('print.txt', 'w') as f:
    print(values, file=f)
