from collections import defaultdict

values = ["president", "requirement", "organ", "sample", "run", "heavy",
        "objective", "experienced", "shallow", "report", "rent", "college",
        "hall", "satellite", "normal", "diamond", "veteran", "glide",
        "unaware", "ambiguity", "twist", "experience", "lump", "fund",
        "relationship", "expectation", "recruit", "stool", "uncertainty"]

result = dict()

for value in values:
    if value[0] in result.keys():
        result[value[0]].append(value)
    else:
        result[value[0]] = [value]

print(result)

result = defaultdict(list)

for value in values:
    result[value[0]].append(value)

print(dict(result))
